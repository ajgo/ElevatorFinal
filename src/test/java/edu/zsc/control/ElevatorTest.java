package edu.zsc.control;

import edu.zsc.logic.OutputMessage;
import edu.zsc.utils.ExcelUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class ElevatorTest {

    @Test
    public void acceptTaskTest() {
        ElevatorManager manager = ElevatorManager.getInstance();
        OutputMessage message = new OutputMessage();
        message.setLift((byte) 1);
        message.setCallFloor((byte) 4);
        message.setTargetFloor((byte) 8);

        manager.handleMessage(message);
        for (int i = 0; i < 500; i++) {
            manager.flushState();
        }
    }

    @Test
    public void excelTest() {
        ElevatorManager manager = ElevatorManager.getInstance();
        List<ElevatorRequest> requests = ExcelUtils.readExcel("Up.xls", ElevatorRequest.class);
        int prevTime = 0;
        for (ElevatorRequest request : requests) {
            int time = request.getTime();
            int flushTime = (time - prevTime) * 10;
            prevTime = time;
            manager.handleMessage(request.toMessage());
            for (int i = 0; i < flushTime; i++) {
                manager.flushState();
            }
        }

        for (int i = 0; i < 1000; i++) {
            manager.flushState();
        }
    }
}
