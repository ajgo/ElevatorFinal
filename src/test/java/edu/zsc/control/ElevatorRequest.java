package edu.zsc.control;

import edu.zsc.ElevatorConfig;
import edu.zsc.logic.OutputMessage;
import lombok.Data;

import java.util.Random;

@Data
public class ElevatorRequest {
    private static final Random random = new Random(System.currentTimeMillis());

    private int time;
    private int startIndex;
    private int endIndex;
    private int lift = random.nextInt(ElevatorConfig.ELEVATOR_COUNT) + 1;

    public OutputMessage toMessage() {
        OutputMessage message = new OutputMessage();
        message.setLift((byte) lift);
        message.setCallFloor((byte) startIndex);
        message.setTargetFloor((byte) endIndex);
        return message;
    }
}
