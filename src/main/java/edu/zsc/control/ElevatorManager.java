package edu.zsc.control;

import edu.zsc.ElevatorConfig;
import edu.zsc.logic.Algorithm;
import edu.zsc.logic.InputMessage;
import edu.zsc.logic.OutputMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 单例对象用于提供给外部调用
 *
 * @author AJinGo, Ymcc
 */
public class ElevatorManager {

    private List<Elevator> elevators;

    private Map<Integer, InputMessage> prevMessages;

    public static final ElevatorManager INSTANCE = new ElevatorManager();

    public static ElevatorManager getInstance() {
        return INSTANCE;
    }

    private ElevatorManager() {
        this.prevMessages = new HashMap<>(ElevatorConfig.ELEVATOR_COUNT);
        this.elevators = new ArrayList<>(ElevatorConfig.ELEVATOR_COUNT);
        for (int i = 0; i < ElevatorConfig.ELEVATOR_COUNT; i++) {
            this.elevators.add(new Elevator(i + 1));
        }
    }

    /**
     * 用于刷新电梯状态
     *
     * @return 电梯详情
     */
    public List<ElevatorDetails> flushState() {
        ElevatorFlushProvider flushProvider = ElevatorFlushProvider.getInstance();
        List<ElevatorDetails> elevatorDetails = new ArrayList<>();
        for (Elevator elevator : elevators) {
            flushProvider.bind(elevator);
            ElevatorDetails details = flushProvider.flush();
            compareMessage(details);
            elevatorDetails.add(details);
            flushProvider.unBind();
        }
        return elevatorDetails;
    }


    /**
     * 用于接收任务
     *
     * @param message OutputMessage
     */
    public void handleMessage(OutputMessage message) {
        int id = message.getLift();
        this.elevators.stream()
                .filter(elevator -> elevator.getId() == id)
                .findFirst()
                .ifPresent(elevator -> elevator.acceptTask(ElevatorTask.fromMessage(message)));
    }

    private void compareMessage(ElevatorDetails details) {
        assert details != null;
        int id = details.getId();
        InputMessage message = details.toMessage();
        InputMessage prevMessage = this.prevMessages.get(id);
        boolean hasDifference = !message.equals(prevMessage);
        if (hasDifference) {
            onElevatorStateChange(message);
            this.prevMessages.put(id, message);
        }
    }


    private void onElevatorStateChange(InputMessage inputMessage) {
        Algorithm.getInstance().handleElevatorStateChange(inputMessage);
    }
}
