package edu.zsc.logic;

import lombok.Data;

/**
 * 算法外层输出给控制层
 */
@Data
public class OutputMessage {
    private byte lift;  //指定派送任务的电梯号
    private byte callFloor; //接载侯悌乘客的楼层
    private byte targetFloor; //指定派送任务的目标楼层（绝对楼层）
}
