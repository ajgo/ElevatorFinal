package edu.zsc.logic;

import lombok.Data;

/**
 * 控制层输入电梯状态到算法外层
 */
@Data
public class InputMessage {
    private byte direction;
    private byte level;
    private byte[] target;
    private byte lift;
    private byte limitFloor;
    private byte[] upFloor;
    private byte[] downFloor;
}
