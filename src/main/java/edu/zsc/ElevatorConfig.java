package edu.zsc;

/**
 * 楼宇电梯固定属性
 */
public class ElevatorConfig {

    // 负载人数
    public static int STAN_COUNT = 10;

    // 平均停靠时间
    public static double STOP_TIME = 4.0;

    // 电梯运行的平均速度
    public static final double SPEED = 2;

    // 电梯最高楼层
    public static int FLOOR_COUNT = 10;

    // 电梯的数量
    public static int ELEVATOR_COUNT = 4;

    public static double EACH_FLOOR_HEIGHT = 4;

    // 电梯刷新的时间间隔
    public static double FLUSH_TIME = 0.1;

//    //楼宇电梯固定属性
//    private int F;//楼层数
//    private int N;//电梯数
//    private int StanNum;//荷载人数
//    private float v;//平均运行速度
//    private float H;//楼层高度
//    private float T;//平均停靠时间

}
